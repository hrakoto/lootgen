#ifndef GFX_H 
#define GFX_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <vector>

using namespace std;

SDL_Texture* loadTexture(const string &file, SDL_Renderer *renderer);
void renderTexture(SDL_Texture *texture, SDL_Renderer *renderer, int x, int y);
void renderTile(SDL_Texture *texture, SDL_Renderer *renderer, int tile_w, int tile_h, int num_x, int num_y, int dst_x, int dst_y, int zoom = 1);
void renderAnimation(SDL_Texture *texture, SDL_Renderer *renderer, vector<SDL_Rect> anim_frames, int frame, int dst_x, int dst_y);
int centerX(SDL_Texture *texture, int screen_w);
int centerX(int w, int screen_w);

#endif
