#ifndef ITEM_H 
#define ITEM_H
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <map>
#include "../headers/font.h"

using namespace std;

/*
 * Mod is used like a structure.
 */
class Mod
{
public:
	Mod(){};
	Mod(int input_id);
	// used in loot table
	int id;
	int rarity;
	// used in display
	int value;
	string name;
};

struct Effect_chunk
{
	string message;
	int stat;
	int value;
	int type;
};

struct Loot_chunk
{
	int id;
	int range_low;
	int range_high;
	vector<Mod> mods;
	// Absolute rarity
	int rarity;
	// Relative rarity for the tier
	int rarity_tier;
};

class Item
{
private:
	SDL_Point item_tex;
	//SDL_Texture *item_sheet;
	Font *font;

	// The drop coordinates are fixed at generation time
	int ground_x;
	int ground_y;
	int inventory_x;
	int inventory_y;
	int w;
	int h;
	int id;
	int type;
	string name;
	string desc;
	bool hasTexLoaded = false;
	vector<Mod> mods;
	bool fully_generated;
	SDL_Rect hitbox;

public:
	Item(Loot_chunk partial_info, int input_x, int input_y);
	void displayGround(SDL_Renderer *renderer, SDL_Texture *item_sheet);
	void displayInventoryFull(SDL_Renderer *renderer, int x, int y);
	void displayInventoryName(SDL_Renderer *renderer, int x, int y);
	void cleanup();
	int getId() { return this->id; }
	string getName() { return this->name; }
	void setGenerated(bool status) { this->fully_generated = status;; }
	bool isGenerated() { return this->fully_generated; }
	vector<Effect_chunk> getEffects();	
	SDL_Rect* getHitbox() { return &this->hitbox; }
};

class Lootgen
{
	vector<Loot_chunk> loot_table;
	vector<int> allItems;
	vector<Loot_chunk> generateLootTable(int level);
public:
	Lootgen(int level);
	Item generateItem(int drop_x, int drop_y);
	
	int generateLootRange(int previous_loot_chance, int rarity_tier, int level);
	void dumpLootTable();
};


#endif
