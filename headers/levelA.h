#ifndef LEVELA_H 
#define LEVELA_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <vector>
#include "../headers/map.h"
#include "../headers/scene.h"
#include "../headers/anim.h"
#include "../headers/player.h"
#include "../headers/gui.h"
#include "../headers/item.h"

using namespace std;
class LevelA : public Scene
{
private:
	SDL_Texture *tileset_tex;
	SDL_Texture *player_tex;
	SDL_Texture *monster_tex;
	SDL_Texture *item_sheet;

	Gui *GUI;

	Map *level_map;
	Lootgen *lootgen;

	Player *player;
	vector<Monster*> monster;
	vector<Item> item_ground;
	int population;
	SDL_Point drop_coords;

public:
	LevelA(SDL_Renderer *renderer, int input_population);
	void display();
	void cleanup();
	Scene_chunk handleKeydown(SDL_Keycode key);
	void handleKeyup(SDL_Keycode key);
	void handleRepeatKeys();
	int move(int pos_x, int pos_y);
	void checkCollisions();
	void compute();
};

#endif
