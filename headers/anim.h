#ifndef ANIM_H 
#define ANIM_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <vector>

using namespace std;
/*
 * - Anim object stands for storing individual anim name & frames!
 * (1 animation = 1 object)
 * - Anim vectors MUST be stored in an external object!
 * - Anim sheet MUST be a line!!!!
 * - The texture is not here !!! These are just coords!!
 */
class Anim
{
private:
	string name;
	vector <SDL_Rect> data;
	// Hopefully the anim frames have equal sizes..?
	int sprite_w;
	int sprite_h;
	int frames;
public:
	Anim(string anim_name, int sheet_x, int sheet_y, int anim_w, int anim_h, int anim_frames);
	string getName() { return this->name; }
	int getFrames() { return this->frames; }
	vector <SDL_Rect> getAnimData() { return this->data; }
};

#endif
