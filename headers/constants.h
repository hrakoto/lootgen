#ifndef CONSTANTS_H 
#define CONSTANTS_H 

#include <iostream>

extern const char *GAME_NAME;
extern const int SCREEN_W;
extern const int SCREEN_H;
extern const int GAME_H;
extern const int GAME_W;
extern const int TILE_W;
extern const int TILE_H;

extern const int BASIC_LIFE;
extern const int BASIC_SPEED;
extern const int BASIC_MISSILE_TIMER;
extern const int BASIC_MISSILE_MIN_DAMAGE;
extern const int BASIC_MISSILE_MAX_DAMAGE;

// Directions
const int DIR_UP = 0;
const int DIR_RIGHT = 1;
const int DIR_DOWN = 2;
const int DIR_LEFT = 3;

// Items ids
const int ID_GUN = 1;
const int ID_DRUG = 2;
const int ID_HAM = 3;
const int ID_BOOTS = 4;
const int ID_ARMOR = 5;
const int ID_COMM = 6;
const int ID_GEM = 7;
const int ID_COIN = 8;
const int ID_HELM = 9;
const int ID_EYES = 10;
const int ID_MAGNET = 11;
const int ID_LASER = 12;
const int ID_FRUIT = 13;
const int ID_ALIEN = 14;
const int ID_BOX = 15;
const int ID_NUCLEAR = 16;

// Mods ids
const int BOOST_SPEED = 1;
const int BOOST_GOLD = 2;
const int BOOST_LIFE = 3;
const int BOOST_MAX_LIFE = 4;
const int BOOST_MIN_DAMAGE = 5;
const int BOOST_MAX_DAMAGE = 6;
const int WEAP_STOMP = 7;
const int WEAP_MISSILE = 8;
const int WEAP_BOMB = 9;
const int WEAP_SPRINT = 10;

// Rarity ids
const int RARITY_POOREST = 1;
const int RARITY_POOR = 2;
const int RARITY_NORMAL = 3;
const int RARITY_GOOD = 4;
const int RARITY_BEST = 5;

// Rarity tiers
const int RARITY_COMMON = 1;
const int RARITY_UNCOMMON = 2;
const int RARITY_RARE = 3;

// Effect types
const int EFFECT_IMMEDIATE = 1;
const int EFFECT_MUST_EQUIP = 2;
const int EFFECT_MUST_HOLD = 3;

// Stats
const int STAT_LIFE = 1;
const int STAT_GOLD = 2;
const int STAT_SPEED = 3;
const int STAT_MIN_DAMAGE = 4;
const int STAT_MAX_DAMAGE = 5;


#endif
