#ifndef MENU_H 
#define MENU_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <vector>
#include "../headers/map.h"
#include "../headers/scene.h"
#include "../headers/font.h"

using namespace std;
class Menu : public Scene
{
private:
	SDL_Texture *background_tex;
	SDL_Texture *background_earth;
	SDL_Texture *title_tex;
	SDL_Texture *tileset_tex;
	vector<SDL_Rect> big_stars;
	vector<SDL_Rect> small_stars;
	Font *font;

public:
	Menu(SDL_Renderer *renderer);
	void display();
	void cleanup();
	Scene_chunk handleKeydown(SDL_Keycode key);
	void handleKeyup(SDL_Keycode key) {};
	void handleRepeatKeys() {};
	void checkCollisions() {};
	void compute() {};

};

#endif
