#ifndef MAP_H 
#define MAP_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <vector>

using namespace std;
class Map
{
private:
	SDL_Texture *spritesheet;
	int spritesheet_tiles_x;
	int spritesheet_tiles_y;
	int tile_w;
	int tile_h;
	int w;
	int h;
	int zoom;
	// Contains the tile index
	vector< vector<int> > mapindex;

	// An actor cannot pass a solid tile
	vector<int> solid_index;

public:
	Map(){};
	// Fake map (in menus)
	Map(SDL_Texture *texture, int num_tiles_x, int num_tiles_y, int clip_w, int clip_h, int map_w, int map_h, int mult = 1);
	// Real map with solid tiles
	Map(SDL_Texture *texture, int num_tiles_x, int num_tiles_y, int clip_w, int clip_h, int map_w, int map_h, vector<int> solid, int mult = 1);
	void generateMap();
	void generateMap(SDL_Point spawn_zone, vector< vector<int> > addon_zones);
	void renderMap(SDL_Renderer *renderer, int origin_x, int origin_y);
	void renderAnimateMap(SDL_Renderer *renderer, int origin_x, int origin_y);
	void addZone(SDL_Point zone_start, vector<int> zone);
	/*
	 * Returns true if the requested position is solid
	 */
	bool isSolid(int x, int y);
};

#endif
