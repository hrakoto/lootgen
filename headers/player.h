#ifndef PLAYER_H
#define PLAYER_H
#include <SDL.h>
#include <cmath>
#include <SDL_image.h>
#include <iostream>
#include "../headers/actor.h"
#include "../headers/item.h"

using namespace std;

class Missile
{
private:
	int x;
	int y;
	int speed;
	int current_timer;
	int ref_timer;
	int alive;
	int direction;
	int min_damage;
	int max_damage;
	bool hit_something;
	Actor *actor;
public:
	Missile(SDL_Renderer *renderer);
	SDL_Rect* getHitbox() { return &this->actor->hitbox; }
	void display(SDL_Renderer *renderer);
	void cleanup() { this->actor->cleanup(); }
	int getX() { return this->actor->pos_x; }
	int getY() { return this->actor->pos_y; }
	int getW() { return this->actor->hitbox.w; }
	int getH() { return this->actor->hitbox.h; }
	int getSpeed() { return this->speed; }
	void setX(int x) { this->actor->pos_x = x; }
	void setY(int y) { this->actor->pos_y = y; }
	void setSpeed(int speed) { this->speed = speed; }
	void setDirection(int direction) { this->direction = direction; }
	int getDamage() { return random(min_damage, max_damage); }
	void setMinDamage(int damage) { this->min_damage = damage;  }
	void setMaxDamage(int damage) { this->max_damage = damage;  }
	int getMinDamage() { return this->min_damage; }
	int getMaxDamage() { return this->max_damage; }
	void setHitSomething(bool value) { this->hit_something = value; }
	bool hasHitSomething() { return this->hit_something; }

};


class Player
{
private:
	int speed;
	int life;
	int gold;
	int last_direction;
	Actor *actor;
	Missile *missile;
	Timer *missileDelay;
	SDL_Rect visual_timer;
	vector<Item> inventory;
public:
	Player(SDL_Renderer *renderer, int input_speed, int input_life, SDL_Point spawn, string anim);
	SDL_Rect* getHitbox() { return &this->actor->hitbox; }
	int getX() { return this->actor->pos_x; }
	int getY() { return this->actor->pos_y; }
	int getW() { return this->actor->hitbox.w; }
	int getH() { return this->actor->hitbox.h; }
	int getCenterX() { return floor(this->getW() / 2) + this->getX(); }
	int getCenterY() { return floor(this->getH() / 2) + this->getY(); }
	void setX(int x) { this->actor->pos_x = x; }
	void setY(int y) { this->actor->pos_y = y; }
	int getSpeed() { return this->speed; }
	int getLife() { return this->life; }
	int getGold() { return this->gold; }
	int getLastDirection() { return this->last_direction; }
	void setLastDirection(int dir) { this->last_direction = dir; }

	void setSpeed(int speed) { this->speed = speed; }
	void setGold(int gold) { this->gold = gold; }
	void setLife(int life) { this->life = life; }
	void display(SDL_Renderer *renderer, int new_x, int new_y);
	void changeAnim(string name) { this->actor->changeAnim(name); }
	void cleanup();
	void move(int direction);
	void fire(int direction);
	int getDamage() { return this->missile->getDamage(); }
	void setMinDamage(int damage) { this->missile->setMinDamage(damage); }
	void setMaxDamage(int damage) { this->missile->setMaxDamage(damage); }
	int getMinDamage() { return this->missile->getMinDamage(); }
	int getMaxDamage() { return this->missile->getMaxDamage(); }
	SDL_Rect* getMissileHitbox() { return this->missile->getHitbox(); };
	void destroyMissile();
	void setMissileHit(bool value) { this->missile->setHitSomething(value); }
	bool hasMissileHit() { return this->missile->hasHitSomething(); }
	void setStat(int stat, int value);
};

class Hitbar
{
	int total_w;
	int h;
	SDL_Rect bar_ref_life;
	SDL_Rect bar_current_life;
public:
	Hitbar(int w, int h);
	void display(SDL_Renderer *renderer, int new_x, int new_y, int ref_life, int current_life);
};

class Monster
{
private:
	int speed;
	int current_life;
	int ref_life;
	Actor *actor;
	Behavior *AI;
	Hitbar *hitbar;
public:	
	Monster(SDL_Renderer *renderer, string type, int input_speed, int input_life, SDL_Point spawn);
	SDL_Rect* getHitbox() { return &this->actor->hitbox; }
	int getX() { return this->actor->pos_x; }
	int getY() { return this->actor->pos_y; }
	void setX(int x) { this->actor->pos_x = x; }
	void setY(int y) { this->actor->pos_y = y; }

	int getSpeed() { return this->speed; }
	int getLife() { return this->current_life; }
	void setSpeed(int speed) { this->speed = speed; }
	void setLife(int life) { this->current_life = life; }
	void display(SDL_Renderer *renderer, int new_x, int new_y);
	void changeAnim(string name) { this->actor->changeAnim(name);  }
	void cleanup() { this->actor->cleanup(); }
	Actor_chunk move() { return this->AI->move(this->speed); }
};



#endif
