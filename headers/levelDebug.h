#ifndef LEVELDEBUG_H 
#define LEVELDEBUG_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include "../headers/map.h"
#include "../headers/scene.h"
#include "../headers/anim.h"
#include "../headers/player.h"
#include "../headers/font.h"
#include "../headers/item.h"

using namespace std;
class LevelDebug : public Scene
{
private:
	SDL_Texture *monster_tex;
	SDL_Texture *item_sheet;
	SDL_Texture *earth_tex;
	int x;
	int y;
	int w;
	int h;
	Font *font;
	Lootgen *lootgen;
	vector<Item*> items;
	vector<Loot_chunk> loot_table;
	Item *item1;
	//Item *test2;

	Monster *monster;

public:
	LevelDebug(SDL_Renderer *renderer);
	void display();
	void cleanup();
	Scene_chunk handleKeydown(SDL_Keycode key);
	void handleKeyup(SDL_Keycode key);
	void handleRepeatKeys() {};
	void checkCollisions() {};
	void compute() {};
	void debugFonts();
};

#endif
