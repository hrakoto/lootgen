#ifndef GUI_H 
#define GUI_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <vector>
#include "../headers/font.h"
#include "../headers/item.h"

using namespace std;
class Gui
{
private:
	SDL_Texture *gui_tex;
	SDL_Renderer *renderer;
	Font *font;

	// Message box
	int message_x;
	int message_y;
	vector<string> displayed_messages;

	// Inventory
	vector<Item> displayed_items;
	bool display_inventory;
	SDL_Rect inventory_global;
	SDL_Rect inventory_item;
	SDL_Rect inventory_list;

public:
	Gui(SDL_Renderer *input_renderer);
	void display();
	void addMessage(string line);
	void addInventory(Item item);
	void displayMessages();
	void displayGold(int gold);
	void displayLife(int life);
	void displaySpeed(int speed);
	void cleanup();
	void setInventoryStatus(bool status) { this->display_inventory = status; }
	bool getInventoryStatus() { return this->display_inventory; }
	SDL_Renderer *getRenderer() { return this->renderer;}
};

#endif
