#ifndef FONT_H 
#define FONT_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <map>

using namespace std;
class Font
{
private:
	SDL_Texture *font_tex;
	SDL_Renderer *renderer;

	map<string, int> charmap;	
	int font_w;
	int font_h;
	int text_w;
	int text_h;
	int box_w;
	int box_h;
	int zoom;
	string text;
	void loadChars();

public:
	Font(string name, SDL_Renderer *input_renderer);
	void display(int dst_x, int dst_y);
	void displayBox(int dst_x, int dst_y);
	void setText(string text, int zoom);
	void cleanup();
	int getBoxW() { return this->box_w; };
	int getTextW() { return this->text_w; };
};

#endif
