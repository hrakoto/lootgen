#ifndef ACTOR_H 
#define ACTOR_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <map>
#include "../headers/anim.h"

using namespace std;

/*
 * This thing is shared between Actor and Behavior
 */
struct Actor_chunk
{
	SDL_Point coords;
	string anim;
};

/*
 * Behavior defines how AI-controlled things behave.
 */
class Behavior
{
public:
	Behavior(){};
	// Pure virtual or you get a compiler error
	virtual Actor_chunk move(int speed) = 0;
};

class StupidAI : public Behavior
{
private:
	Actor_chunk cur_chunk;
	SDL_Point last_position;

	// holds the current direction, 1000 if we must change
	int current_dir;
public:
	StupidAI(int actor_x, int actor_y);
	Actor_chunk move(int speed);
};

/*
 * Actor can be anything with animations & hitbox & display
 */
class Actor
{
private:
	SDL_Texture *actor_tex;
	map<string, Anim*> anims;
	Anim *current_anim;

	void setCurrentAnim(Anim *anim);
public:
	Actor(SDL_Texture *texture, int input_w, int input_h);
	void display(SDL_Renderer *renderer, int new_x, int new_y);
	void cleanup();
	// TODO: put private and use ALWAYS setter and maybe getters
	int pos_x;
	int pos_y;
	void setPositionXY(int x, int y);
	void setPositionX(int x);
	void setPositionY(int x);
	int current_frame;
	int tempo_frame;
	void changeAnim(string name);
	void addAnim(string name, int x, int y, int w, int h, int frames);
	Anim* getCurrentAnim() { return this->current_anim; }
	Behavior* setBehavior(string input_name);
	SDL_Rect hitbox;

};

#endif
