#ifndef TOOLS_H 
#define TOOLS_H 
#include <SDL.h>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;


void log(string type, string msg);
void log(string type, int value);
int random(int min, int max);
string monoxor(string text, char key);
vector<int> splitToInt(string str, char delimiter);

template<typename T>
void pop_front(vector<T>& vec)
{
	if(!vec.empty())
	{
		vec.erase(vec.begin());
   	}
}

class Timer
{
private:
	Uint32 current_time;
	Uint32 start_time;
	bool started;
public:
	Timer();
	void start();
	void stop();
	bool isStarted();
	Uint32 getTime();
};

#endif
