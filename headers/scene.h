#ifndef SCENE_H 
#define SCENE_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <vector>

using namespace std;

/*
 * This thing is used to invoke scenes with parameters
 */
struct Scene_chunk
{
	string name;
	int population;
	bool empty;
};


class Scene
{
private:
	 // Name: used to invoke the right child class in factory
	string name;
	SDL_Renderer* renderer;
public:
	Scene(){};
	Scene(SDL_Renderer *renderer) { this->setRenderer(renderer); }
	virtual void display(){};
	string getName() { return name; }
	void setName(string input) { name = input; }
	void setRenderer(SDL_Renderer* input_renderer) { renderer = input_renderer; }
	SDL_Renderer* getRenderer() { return renderer; }
	void cleanup(){};
	virtual Scene_chunk handleKeydown(SDL_Keycode key) = 0;
	virtual void handleKeyup(SDL_Keycode key) = 0;
	virtual void handleRepeatKeys() = 0;
	virtual void checkCollisions() = 0;
	virtual void compute() = 0;
};

#endif
