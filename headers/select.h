#ifndef SELECT_H 
#define SELECT_H 
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include "../headers/map.h"
#include "../headers/scene.h"
#include "../headers/font.h"

using namespace std;
class Select : public Scene
{
private:
	SDL_Texture *background_tex;
	SDL_Texture *fields_tex;
	SDL_Texture *mountains_tex;
	SDL_Texture *title_tex;
	SDL_Texture *selector_tex;
	SDL_Texture *fields_name;
	SDL_Texture *mountains_name;
	vector<SDL_Rect> big_stars;
	vector<SDL_Rect> small_stars;
	Map *fields_map;
	Map *mountains_map;
	Font *font;
	int selector_index_x;
	int population_A;
	int population_B;
public:
	Select(SDL_Renderer *renderer);
	void display();
	void displaySelector();
	void clearSelector();
	void cleanup();
	Scene_chunk handleKeydown(SDL_Keycode key);
	void handleKeyup(SDL_Keycode key) {};
	void handleRepeatKeys() {};
	void checkCollisions() {};
	void compute() {};
};

#endif
