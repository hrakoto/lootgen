#!/bin/bash

echo "== Cleaning previous build"
make clean

if [ "$1" == "debian" ]; then
	echo "== Build for $1"
	export LD_LIBRARY_PATH="/usr/local/lib"
	export TARGET_COMPILER="g++"
	export TARGET_BINARY="loot-linux32"
	export COMPILFLAGS="-I/usr/local/include/SDL2"
	time make
elif [ "$1" == "windows" ]; then
	echo "== Build for $1"
	export TARGET_COMPILER="/usr/local/cross-tools/bin/x86_64-w64-mingw32-g++"
	export TARGET_BINARY="loot-win64.exe"
	export COMPILFLAGS="`/usr/local/cross-tools/x86_64-w64-mingw32/bin/sdl2-config --cflags`"
	export LFLAGS="`/usr/local/cross-tools/x86_64-w64-mingw32/bin/sdl2-config --libs` -static-libgcc -static-libstdc++"
	time make
	cp loot-win64.exe /vagrant_data
else
	echo "== Build for default (crunchbang)"
	export LD_LIBRARY_PATH="/usr/local/lib"
	export TARGET_COMPILER="g++"
	export TARGET_BINARY="loot-linux32"
	export COMPILFLAGS="-I/usr/include/SDL2"
	export LFLAGS="-lSDL2"
	time make
fi
