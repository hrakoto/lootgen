# Project: lootgen
# Makefile 

# note: target_compiler, target_binary and compilflags are env variables, see make.sh

#COMPILER=g++ -Xlinker --verbose
COMPILER=$(TARGET_COMPILER)
CFLAGS=$(COMPILFLAGS) -Wall -c -std=c++11 -D_REENTRANT
#LDFLAGS=-lSDL2 -lSDL2_image
LDFLAGS=$(LFLAGS) -lSDL2_image
SRC=$(wildcard src/*.cpp)
OBJ=$(addprefix obj/,$(notdir $(SRC:.cpp=.o)))
EXE=$(TARGET_BINARY)
all: $(EXE)

$(EXE): $(OBJ)
	$(COMPILER) $(OBJ) -o $(EXE) $(LDFLAGS) 

obj/%.o: src/%.cpp
	$(COMPILER) $(CFLAGS) -c -o $@ $<

clean:
	rm -rf obj/*.o loot debug.log
