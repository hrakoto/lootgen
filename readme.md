# Lootgen
Lootgen is a 2D "action-rpg" coded in 1 month. The game is playable on Windows 64 and Linux 32.

![menu screen](http://i.imgur.com/xdj6jbg.png)![game screen](http://i.imgur.com/Q5C9Rk7.png)

## How to compile

* You need GCC 4.7 for C++11
* You need libfreetype6-dev for SDL_ttf
* You need libpng12-dev for SDL_image

```
./make.sh debian
```
## Credits

* Tiles are from http://pousse.rapiere.fr/tome. Some were modified and combined.
