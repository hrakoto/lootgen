#include <iostream>
#include <vector>
#include <cmath>
#include <typeinfo>
#include <cmath>
#include <SDL_image.h>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/font.h"

using namespace std;

Font::Font(string name, SDL_Renderer *input_renderer): renderer(input_renderer)
{
	if(name == "strider2")
	{
		font_tex = loadTexture("gfx/font_strider2.png", this->renderer);
		this->font_w = 8;
		this->font_h = 8;
		this->loadChars();
	}
	else
	{
		font_tex = nullptr;
	}
}
void Font::cleanup()
{
	SDL_DestroyTexture(font_tex);
}

void Font::setText(string text, int zoom)
{
	this->text = text;	
	this->zoom = zoom;
	// we don't know how it will be displayed so we calculate everything
	this->text_w = (int)text.size() * font_w * zoom;
	this->text_h = font_h * zoom; 
	this->box_w = text_w + (2 * font_w);
	this->box_h = text_h + (2 * font_h);
}

void Font::display(int dst_x, int dst_y)
{
	for(unsigned int i=0; i < this->text.size(); ++i)
	{
		// text[i] contains the ascii code, we must cast it
		string sign;
		sign = (char) this->text[i];

		// get the charmap num
		int num = charmap.at(sign);

		// transform num to x,y INDEXED coordinates
		int index_x = num % 16;
		int index_y = floor(num / 16);
		renderTile(font_tex, renderer, font_w, font_h, index_x, index_y, dst_x + ( i * font_w * zoom), dst_y, zoom);
	
	}
}

void Font::displayBox(int dst_x, int dst_y)
{
	SDL_Rect box = { dst_x, dst_y, box_w, box_h };

	// 1) render the black box
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(renderer, &box);
	// 2) box border
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	SDL_RenderDrawRect(renderer, &box);
	// 3) decorating lines
	SDL_SetRenderDrawColor(renderer, 128, 0, 0, 255);
	SDL_RenderDrawLine(renderer, dst_x, dst_y + box_h, dst_x + box_w - 1, dst_y + box_h);
	// 4) the text
	this->display(dst_x + font_w, dst_y + font_h);

	// Reset the drawcolor
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}

void Font::loadChars()
{
	this->charmap["0"] = 0; 
	this->charmap["1"] = 1; 
	this->charmap["2"] = 2; 
	this->charmap["3"] = 3; 
	this->charmap["4"] = 4; 
	this->charmap["5"] = 5; 
	this->charmap["6"] = 6; 
	this->charmap["7"] = 7; 
	this->charmap["8"] = 8; 
	this->charmap["9"] = 9; 
	this->charmap["A"] = 10; 
	this->charmap["B"] = 11; 
	this->charmap["C"] = 12; 
	this->charmap["D"] = 13; 
	this->charmap["E"] = 14; 
	this->charmap["F"] = 15; 
	this->charmap[" "] = 16; 
	this->charmap["!"] = 17; 
	this->charmap["\""] = 18; 
	this->charmap["#"] = 19; 
	this->charmap["$"] = 20; 
	this->charmap["%"] = 21; 
	this->charmap["&"] = 22; 
	this->charmap["'"] = 23; 
	this->charmap["("] = 24; 
	this->charmap[")"] = 25; 
	this->charmap["*"] = 26; 
	this->charmap["+"] = 27; 
	this->charmap[","] = 28; 
	this->charmap["-"] = 29; 
	this->charmap["."] = 30; 
	this->charmap["/"] = 31; 
	this->charmap[":"] = 42; 
	this->charmap["<"] = 44; 
	this->charmap["="] = 45; 
	this->charmap[">"] = 46; 
	this->charmap["?"] = 47; 
	this->charmap["G"] = 55; 
	this->charmap["H"] = 56; 
	this->charmap["I"] = 57; 
	this->charmap["J"] = 58; 
	this->charmap["K"] = 59; 
	this->charmap["L"] = 60; 
	this->charmap["M"] = 61; 
	this->charmap["N"] = 62; 
	this->charmap["O"] = 63; 
	this->charmap["P"] = 64; 
	this->charmap["Q"] = 65; 
	this->charmap["R"] = 66; 
	this->charmap["S"] = 67; 
	this->charmap["T"] = 68; 
	this->charmap["U"] = 69; 
	this->charmap["V"] = 70; 
	this->charmap["W"] = 71; 
	this->charmap["X"] = 72; 
	this->charmap["Y"] = 73; 
	this->charmap["Z"] = 74; 
	this->charmap["["] = 75; 
	this->charmap["\\"] = 76; 
	this->charmap["]"] = 77; 
	this->charmap["^"] = 78; 
	this->charmap["_"] = 79; 
	this->charmap["a"] = 81; 
	this->charmap["b"] = 82; 
	this->charmap["c"] = 83; 
	this->charmap["d"] = 84; 
	this->charmap["e"] = 85; 
	this->charmap["f"] = 86;
	this->charmap["g"] = 87; 
	this->charmap["h"] = 88; 
	this->charmap["i"] = 89; 
	this->charmap["j"] = 90; 
	this->charmap["k"] = 91; 
	this->charmap["l"] = 92; 
	this->charmap["m"] = 93; 
	this->charmap["n"] = 94; 
	this->charmap["o"] = 95; 
	this->charmap["p"] = 96; 
	this->charmap["q"] = 97; 
	this->charmap["r"] = 98; 
	this->charmap["s"] = 99; 
	this->charmap["t"] = 100; 
	this->charmap["u"] = 101; 
	this->charmap["v"] = 102; 
	this->charmap["w"] = 103; 
	this->charmap["x"] = 104; 
	this->charmap["y"] = 105; 
	this->charmap["z"] = 106; 
	this->charmap["{"] = 107; 
	this->charmap["|"] = 108; 
	this->charmap["}"] = 109; 
	this->charmap["~"] = 110; 
}
