#include <ctime>
#include <iostream>
#include <random>
#include <vector>
#include <fstream>
#include <sstream>
#include "../headers/tools.h"

using namespace std;

void log(string type, string msg) 
{ 
	ofstream logfile("debug.log", std::ios_base::app | std::ios_base::out);	

	// current time in seconds since epoch
	time_t current_time = time(nullptr);
	char formatted_time[200]={0};
	tm now=*localtime(&current_time);
	const char format[]="%a %b %d %Y %X"; 
	if (strftime(formatted_time, sizeof(formatted_time)-1, format, &now)>0)
	{
		// log to file
		logfile << formatted_time << " (" << type << ") " << msg << endl;

	}
}

bool replace(string& str, const string& from, const string& to) 
{
	size_t start_pos = str.find(from);
	if(start_pos == string::npos)
	{
		return false;
	}
	str.replace(start_pos, from.length(), to);
	return true;
}

void log(string type, int value)
{
	log(type, to_string(value));
}


string monoxor(string toEncrypt, char key)
{
	string output = toEncrypt;
	
	for(int i = toEncrypt.size(); i > 0; i--)
	{
		output[i] = toEncrypt[i] ^ key;
	}
	
	return output;
}

vector<int> splitToInt(string str, char delimiter)
{
	vector<int> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;
	
	while(getline(ss, tok, delimiter))
	{
		internal.push_back(stoi(tok));
	}
	return internal;
}

#if (defined(_WIN32) || defined(_WIN64))
#include <windows.h>
#include <wincrypt.h>
uint64_t getSeed()
{
	uint64_t ret;
	HCRYPTPROV hp = 0;
	CryptAcquireContext(&hp, 0, 0, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);
	CryptGenRandom(hp, sizeof(ret), reinterpret_cast<uint8_t*>(&ret));
	CryptReleaseContext(hp, 0); 
	return ret;
}
#else
int getSeed()
{
	random_device rd;
	return rd();
}
#endif

int random(int min, int max)
{
	mt19937 mt(getSeed());
	uniform_int_distribution<int> dist(min, max);
	
	return dist(mt);
}

//int randomUnique(int min, int max, vector<int> &box)
//{
//	int pick = random(min, max);
//	box.erase(std::remove(box.begin(), box.end(), pick), vec.end());
//	return pick;
//}

// Timer class

Timer::Timer()
{
	log("tools", "Initializing timer");
	current_time = 0;
	started = false;
}
void Timer::start()
{
	start_time = SDL_GetTicks();
	started = true;	
}
void Timer::stop()
{
	if(started)
	{
		start_time = 0;
		started = false;
	}
}
Uint32 Timer::getTime()
{
	Uint32 time = 0;
	if(started)
	{
		time = SDL_GetTicks() - start_time;
	}

	return time;
}
bool Timer::isStarted()
{
	return started;
}

