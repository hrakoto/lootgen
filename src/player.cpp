#include <iostream>
#include <vector>
#include <SDL_image.h>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/player.h"

using namespace std;

/*******************************************************************************
 * PLAYER CLASS
 *******************************************************************************/


Player::Player(SDL_Renderer *renderer, int input_speed, int input_life, SDL_Point spawn, string anim) :	
	speed(input_speed),
	life(input_life),
	gold(0),
	last_direction(DIR_RIGHT)
{
	log("player", "initializing player");
	actor = new Actor(loadTexture("gfx/bowser.png", renderer), 55, 65);
	actor->addAnim("walk_down", 0, 65, 55, 65, 3);
	actor->addAnim("walk_up", 0, 0, 55, 65, 3);
	actor->addAnim("walk_right", 0, 2 * 65, 55, 65, 3);
	actor->addAnim("walk_left", 0, 3 * 65, 55, 65, 3);
	actor->addAnim("idle", 0, 65, 55, 65, 1);
	actor->changeAnim(anim);
	actor->setPositionXY(spawn.x, spawn.y);
	missile = new Missile(renderer);
	missileDelay = new Timer();
	visual_timer.x = 10;
	visual_timer.y = 579;
	visual_timer.h = 10;
	visual_timer.w = 0;

}

void Player::fire(int direction)
{   
	if(missileDelay->isStarted())
	{
		// do nothing
	}
	else
	{
		missileDelay->start();
		missile->setHitSomething(false);
		// Starting the missile
		missile->setX(getCenterX());
		missile->setY(getCenterY());
	
	
		switch(direction)
		{
			case DIR_UP:
				missile->setDirection(DIR_UP);
				break;
			case DIR_RIGHT:
				missile->setDirection(DIR_RIGHT);
				break;
			case DIR_DOWN:
				missile->setDirection(DIR_DOWN);
				break;
				
			case DIR_LEFT:
				missile->setDirection(DIR_LEFT);
				break;
	
			default:
				break;
		}

	}
}

void Player::move(int direction)
{
	if(getY() <= 0)
	{
		direction = DIR_DOWN;
	}
	else if(getX() + 55 >= GAME_W)
	{
		direction = DIR_LEFT;
	}
	else if(getY() + 65 >= GAME_H)
	{
		direction = DIR_UP;
	}
	else if(getX() <= 0)
	{
		direction = DIR_RIGHT;
	}

	switch(direction)
	{
		case DIR_UP:
			setY(getY() - getSpeed());
			this->actor->changeAnim("walk_up");
			break;

		case DIR_RIGHT:
			setX(getX() + getSpeed());
			this->actor->changeAnim("walk_right");
			break;
		case DIR_DOWN:
			setY(getY() + getSpeed());
			this->actor->changeAnim("walk_down");
			break;
			
		case DIR_LEFT:
			setX(getX() - getSpeed());
			this->actor->changeAnim("walk_left");
			break;

		default:
			break;
	}
}

void Player::destroyMissile()
{
	log("player", "missile destroyed");
	missileDelay->stop();
	visual_timer.w = 0;
}

void Player::display(SDL_Renderer *renderer, int new_x, int new_y)
{
	this->actor->display(renderer, new_x, new_y);
	if(missileDelay->isStarted() && missileDelay->getTime() <= (unsigned) BASIC_MISSILE_TIMER * 1000)
	{
		// display missile
		this->missile->display(renderer);

		// render the visual timer
		SDL_SetRenderDrawColor(renderer, 128, 55, 128, 255);
		SDL_RenderFillRect(renderer, &visual_timer);
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

		visual_timer.w = floor((missileDelay->getTime() * 76) / (BASIC_MISSILE_TIMER * 1000));
	}
	else
	{
		if(missileDelay->isStarted())
		{
			missileDelay->stop();
		}
		visual_timer.w = 0;
	}
}

void Player::setStat(int stat, int value)
{
	if(stat == STAT_LIFE)
	{
		int new_value = getLife() + value;
		if(new_value > 0)
		{
			setLife(getLife() + value);
		}
	}
	else if(stat == STAT_GOLD)
	{
		int new_value = getGold() + value;
		if(new_value > 0)
		{
			setGold(getGold() + value);
		}
	}
	else if(stat == STAT_SPEED)
	{

		int new_value = getSpeed() + value;
		if(new_value > 0)
		{
			setSpeed(getSpeed() + value);
		}
	}
	else if(stat == STAT_MIN_DAMAGE)
	{
		int new_value = getMinDamage() + value;
		if(new_value < getMaxDamage())
		{
			setMinDamage(new_value);
		}
	}
	else if(stat == STAT_MAX_DAMAGE)
	{
		int new_value = getMaxDamage() + value;
		if(new_value > getMinDamage())
		{
			setMaxDamage(new_value);
		}
	}
}

void Player::cleanup()
{ 
	actor->cleanup(); 
	missile->cleanup();
}

/*******************************************************************************
 * MISSILE CLASS
 *******************************************************************************/


Missile::Missile(SDL_Renderer *renderer) :	
	speed(BASIC_SPEED),
	current_timer(BASIC_MISSILE_TIMER),
	ref_timer(BASIC_MISSILE_TIMER),
	min_damage(BASIC_MISSILE_MIN_DAMAGE),
	max_damage(BASIC_MISSILE_MAX_DAMAGE),
	hit_something(false)
{
	log("missile", "Initializing missile");
	actor = new Actor(loadTexture("gfx/missiles.png", renderer), 32, 32);
	actor->addAnim("basic", 0, 32, 32, 32, 1);
	actor->addAnim("fire", 32, 32, 32, 32, 1);
	actor->addAnim("iron", 64, 32, 32, 32, 1);
	actor->addAnim("bomb", 96, 32, 32, 32, 1);
	actor->changeAnim("fire");
}

void Missile::display(SDL_Renderer *renderer)
{
	// the player has already set the original missile position
	actor->display(renderer, getX(), getY());
	switch(direction)
	{
		case DIR_UP:
			setY(getY() - getSpeed());
			break;
		case DIR_RIGHT:
			setX(getX() + getSpeed());
			break;
		case DIR_DOWN:
			setY(getY() + getSpeed());
			break;
		case DIR_LEFT:
			setX(getX() - getSpeed());
			break;
	}
}

/*******************************************************************************
 * MONSTER CLASS
 *******************************************************************************/

Monster::Monster(SDL_Renderer *renderer, string type, int input_speed, int input_life, SDL_Point spawn)	: 
	speed(input_speed),
	current_life(input_life),
	ref_life(input_life)
{
	int w;
	int h;
	int frames;
	string file;
	string behavior;

	if(type == "mario")
	{
		file = "gfx/mario.png";
		w = 18;
		h = 22;
		frames = 3;
		behavior = "stupid";
	}

	// Load the texture and set up the actor
	this->actor = new Actor(loadTexture("gfx/mario.png", renderer), w, h);

	// Register animations
	this->actor->addAnim("walk_right", 0, 0, w, h, frames);
	this->actor->addAnim("walk_up", 0, h, w, h, frames);
	this->actor->addAnim("walk_down", 0, 2 * h, w, h, frames);
	this->actor->addAnim("walk_left", 0, 3 * h, w, h, frames);
	this->actor->changeAnim("walk_down");

	this->actor->setPositionXY(spawn.x, spawn.y);

	this->AI = this->actor->setBehavior(behavior);

	// hitbar
	hitbar = new Hitbar(this->actor->hitbox.w, 2);

	}

void Monster::display(SDL_Renderer *renderer, int new_x, int new_y)
{
	// display the monster sprite
	this->actor->display(renderer, new_x, new_y);

	if(getLife() < ref_life && getLife() > 0)
	{
		hitbar->display(renderer, this->actor->pos_x, this->actor->pos_y - 4, ref_life, current_life);
	}
}

Hitbar::Hitbar(int input_w, int input_h)
	: total_w(input_w),
	h(input_h)
{
	bar_ref_life.h = h;
	bar_current_life.h = h;
}

void Hitbar::display(SDL_Renderer *renderer, int new_x, int new_y, int ref_life, int current_life)
{
	// current life dimensions
	bar_current_life.x = new_x;
	bar_current_life.y = new_y;
	bar_current_life.w = floor(current_life * total_w / ref_life);

	// ref life dimensions
	bar_ref_life.x = new_x + bar_current_life.w;
	bar_ref_life.y = new_y;
	bar_ref_life.w = total_w - bar_current_life.w;

	// paint green
	SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
	SDL_RenderFillRect(renderer, &bar_current_life);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

	// paint red
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	SDL_RenderFillRect(renderer, &bar_ref_life);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

}
