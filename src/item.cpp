#include <iostream>
#include <vector>
#include <cmath>
#include <SDL_image.h>
#include <string>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/item.h"

using namespace std;

Item::Item(Loot_chunk partial_info, int input_x, int input_y) :
	ground_x(input_x),
	ground_y(input_y),
	w(TILE_W),
	h(TILE_H),
	id(partial_info.id),
	fully_generated(false),
	hasTexLoaded(false),
	font(nullptr),
	mods(partial_info.mods)
{

	// hitbox
	this->hitbox.x = ground_x;
	this->hitbox.y = ground_y;
	this->hitbox.w = w;
	this->hitbox.h = h;

	// Name and texture
	switch(id)	
	{
		case ID_GUN:
			item_tex = { 0, 0 };
			this->name = "Gun";
		break;
		case ID_DRUG:
			item_tex = { 1, 0 };
			this->name = "Antibiotics";
		break;
		case ID_HAM:
			item_tex = { 2, 0 };
			this->name = "Ham";
		break;
		case ID_BOOTS:
			item_tex = { 3, 0 };
			this->name = "Boots";
		break;
		case ID_ARMOR:
			item_tex = { 0, 1 };
			this->name = "Armor";
		break;
		case ID_COMM:
			item_tex = { 1, 1 };
			this->name = "Communicator";
		break;
		case ID_GEM:
			item_tex = { 2, 1 };
			this->name = "Gems";
		break;
		case ID_COIN:
			item_tex = { 3, 1 };
			this->name = "Coins";
		break;
		case ID_HELM:
			item_tex = { 0, 2 };
			this->name = "Helm";
		break;
		case ID_EYES:
			item_tex = { 1, 2 };
			this->name = "Demon eyes";
		break;
		case ID_MAGNET:
			item_tex = { 2, 2 };
			this->name = "Magnet";
		break;
		case ID_LASER:
			item_tex = { 3, 2 };
			this->name = "Laser cannon";
		break;
		case ID_FRUIT:
			item_tex = { 0, 3 };
			this->name = "Fresh fruit";
		break;
		case ID_ALIEN:
			item_tex = { 1, 3 };
			this->name = "Alien device";
		break;
		case ID_BOX:
			item_tex = { 2, 3 };
			this->name = "Surprise package";
		break;
		case ID_NUCLEAR:
			item_tex = { 3, 3 };
			this->name = "Nuclear bomb";
		break;
	}

	// Complete the mod values
	for(int j=0; j<mods.size(); j++)
	{
		switch(mods[j].id)
		{
			case(BOOST_SPEED):
				switch(mods[j].rarity)
				{
					case RARITY_POOREST:
						mods[j].value = -2;
					break;
					case RARITY_POOR:
						mods[j].value = -1;
					break;
					case RARITY_NORMAL:
						mods[j].value = 0;
					break;
					case RARITY_GOOD:
						mods[j].value = 1;
					break;
					case RARITY_BEST:
						mods[j].value = 2;
					break;
				}
			break;	
			case(BOOST_GOLD):
				switch(mods[j].rarity)
				{
					case RARITY_POOREST:
						mods[j].value = random(-1000,-501);
					break;
					case RARITY_POOR:
						mods[j].value = random(-500, 50);
					break;
					case RARITY_NORMAL:
						mods[j].value = random(-100,100);
					break;
					case RARITY_GOOD:
						mods[j].value = random(1,1000);
					break;
					case RARITY_BEST:
						mods[j].value = random(1000,5000);
					break;
				}
			break;	
			case(BOOST_LIFE):
				switch(mods[j].rarity)
				{
					case RARITY_POOREST:
						mods[j].value = random(-50,-11);
					break;
					case RARITY_POOR:
						mods[j].value = random(-10,5);
					break;
					case RARITY_NORMAL:
						mods[j].value = random(1,10);
					break;
					case RARITY_GOOD:
						mods[j].value = random(1,50);
					break;
					case RARITY_BEST:
						mods[j].value = random(50,100);
					break;
				}
			break;	
			case(BOOST_MAX_LIFE):
				switch(mods[j].rarity)
				{
					case RARITY_POOREST:
						mods[j].value = random(-10,-6);
					break;
					case RARITY_POOR:
						mods[j].value = random(-5,0);
					break;
					case RARITY_NORMAL:
						mods[j].value = random(1,9);
					break;
					case RARITY_GOOD:
						mods[j].value = random(10,14);
					break;
					case RARITY_BEST:
						mods[j].value = random(15,20);
					break;
				}
			break;	
			case(BOOST_MIN_DAMAGE):
				switch(mods[j].rarity)
				{
					case RARITY_POOREST:
						mods[j].value = random(-10,-5);
					break;
					case RARITY_POOR:
						mods[j].value = random(-4,2);
					break;
					case RARITY_NORMAL:
						mods[j].value = random(1,4);
					break;
					case RARITY_GOOD:
						mods[j].value = random(5,10);
					break;
					case RARITY_BEST:
						mods[j].value = random(10, 15);
					break;

				}
			break;	
			case(BOOST_MAX_DAMAGE):
				switch(mods[j].rarity)
				{
					case RARITY_POOREST:
						mods[j].value = random(-10,-5);
					break;
					case RARITY_POOR:
						mods[j].value = random(-4,4);
					break;
					case RARITY_NORMAL:
						mods[j].value = random(1,10);
					break;
					case RARITY_GOOD:
						mods[j].value = random(10,15);
					break;
					case RARITY_BEST:
						mods[j].value = random(16, 25);
					break;
				}
			break;	
		}
	}
}

vector<Effect_chunk> Item::getEffects()
{
	vector<Effect_chunk> result;
	for(int i=0; i<mods.size(); i++)
	{
		Effect_chunk tmp;
		tmp.value = mods[i].value;
		log("item", "mod = " + to_string(mods[i].id));
		log("item", "value = " + to_string(mods[i].value));
		switch(mods[i].id)
		{
			// Coins: immediate effect
			case BOOST_GOLD:
				{
					tmp.stat = STAT_GOLD;
					tmp.type = EFFECT_IMMEDIATE;
					if(tmp.value > 0)
					{
						tmp.message = "You gain " + to_string(tmp.value) + " coins";
					}
					else
					{
						tmp.message = "Counterfeit! You lose " + to_string(tmp.value) + " coins";
					}
				}
				break;
			// Life: immediate effect
			case BOOST_LIFE:
				{
					tmp.stat = STAT_LIFE;
					tmp.type = EFFECT_IMMEDIATE;
					if(tmp.value > 0)
					{
						tmp.message = "You heal " + to_string(tmp.value) + " life";
					}
					else
					{
						tmp.message = "Yuck! You lose " + to_string(tmp.value) + " life";
					}
				}
			break;
			default:
				tmp.type = EFFECT_IMMEDIATE;
				tmp.message = name + " has no effect (yet)";
			break;
		}
		result.push_back(tmp);
	}
	return result;
}

void Item::displayGround(SDL_Renderer *renderer, SDL_Texture *item_sheet)
{
	renderTile(item_sheet, renderer, w, h, item_tex.x, item_tex.y, ground_x, ground_y, 1);
}

void Item::displayInventoryFull(SDL_Renderer *renderer, int x, int y)
{
	if(!font)
	{
		font = new Font("strider2", renderer);
	}

	font->setText(this->name, 2);
	font->display(x,y);
	string display_value;
	for(int i=0; i<mods.size(); i++)
	{
		if(mods[i].value > 0)
		{
			display_value = "+" + to_string(mods[i].value);
		}
		else
		{
			display_value = to_string(mods[i].value);
		}

		font->setText(display_value + " " + mods[i].name, 2);
		font->display(x,  y + 16 + (16 * i));
	}
}
void Item::displayInventoryName(SDL_Renderer *renderer, int x, int y)
{
	if(!font)
	{
		font = new Font("strider2", renderer);
	}
	font->setText(this->name, 2);
	font->display(x,y);

}
void Item::cleanup()
{
	// Item texture is not cleared here, this is normal
	font->cleanup();
}

/*******************************************************************************
 * Class Lootgen
 *******************************************************************************/

Lootgen::Lootgen(int level)
{

	loot_table =  this->generateLootTable(level);
}

Item Lootgen::generateItem(int drop_x, int drop_y)
{
	int roll = random(0,99);

	// simplified version
	// TODO: take into account modifier and day
	for(unsigned int i=0; i < this->loot_table.size(); i++)
	{
		if(roll >= this->loot_table[i].range_low && roll <= this->loot_table[i].range_high)
		{
			Item test(this->loot_table[i], drop_x, drop_y);
			test.setGenerated(true);
			log("item", test.getName() + " will be generated at " + to_string(drop_x) + " " + to_string(drop_y));
			return test;
		}
	}
}

/*
 * WARNING:
 * Everything is generated manually. You MUST ensure
 * you have the right rarity and item numbers in the table!
 */
vector<Loot_chunk> Lootgen::generateLootTable(int level)
{
	log("item", "enter generateLootTable");
	int rarity_common;
	int rarity_uncommon;
	int rarity_rare;
	// Which items are available in this tier
	vector<int> available_items;
	int loot_range = 0;
	vector<Loot_chunk> loot_table;

	// Each level has its drop table
	available_items.empty();
	switch(level)
	{
		case 1:
			rarity_common = RARITY_NORMAL;
			rarity_uncommon = RARITY_POOR;
			rarity_rare = RARITY_POOREST;
			available_items.push_back(ID_COIN);
			available_items.push_back(ID_DRUG);
			available_items.push_back(ID_FRUIT);
			available_items.push_back(ID_GUN);
			available_items.push_back(ID_HELM);
			break;
		case 2:
			rarity_common = RARITY_NORMAL;
			rarity_uncommon = RARITY_GOOD;
			rarity_rare = RARITY_BEST;
			available_items.push_back(ID_COIN);
			available_items.push_back(ID_MAGNET);
			available_items.push_back(ID_HAM);
			available_items.push_back(ID_GEM);
			available_items.push_back(ID_HELM);
			available_items.push_back(ID_NUCLEAR);
			break;
	}

	// Prepare each item skeleton
	for(int i=0; i<available_items.size(); i++)
	{
		Loot_chunk item_skeleton;
		log("item", "enter the item by item loop");

		item_skeleton.id = available_items[i];
		switch(item_skeleton.id)
		{
			case ID_COIN:
			case ID_GEM:
				{
				Mod tmp(BOOST_GOLD);
				tmp.rarity = rarity_common;
				item_skeleton.rarity_tier = RARITY_COMMON;
				item_skeleton.mods.push_back(tmp);
				}
				break;
			case ID_DRUG:
			case ID_MAGNET:
				{
				Mod tmp(BOOST_SPEED);
				tmp.rarity = rarity_uncommon;
				item_skeleton.rarity_tier = RARITY_UNCOMMON;
				item_skeleton.mods.push_back(tmp);
				}
				break;
			case ID_FRUIT:
			case ID_HAM:
				{
				Mod tmp(BOOST_LIFE);
				tmp.rarity = rarity_common;
				item_skeleton.rarity_tier = RARITY_COMMON;
				item_skeleton.mods.push_back(tmp);
				}
				break;
			case ID_HELM:
				{
				Mod tmp(BOOST_MAX_LIFE);
				tmp.rarity = rarity_uncommon;
				item_skeleton.rarity_tier = RARITY_UNCOMMON;
				item_skeleton.mods.push_back(tmp);
				}
				break;
			case ID_GUN:
				{
				Mod tmp(WEAP_MISSILE);
				tmp.rarity = rarity_uncommon;
				item_skeleton.rarity_tier = RARITY_UNCOMMON;
				item_skeleton.mods.push_back(tmp);
				}
				break;
			case ID_NUCLEAR:
				{
				Mod tmp(WEAP_BOMB);
				tmp.rarity = rarity_rare;
				item_skeleton.rarity_tier = RARITY_RARE;
				item_skeleton.mods.push_back(tmp);
				}
				break;
		}

		item_skeleton.range_low = loot_range;
		//log("item", "range low= " + to_string(item_skeleton.range_low));
		item_skeleton.range_high = generateLootRange(loot_range, item_skeleton.rarity_tier, level) - 1;
		//log("item", "range high= " + to_string(item_skeleton.range_high));
		loot_range = item_skeleton.range_high + 1;
		
		loot_table.push_back(item_skeleton);
	}


	return loot_table;
}

int Lootgen::generateLootRange(int previous_loot_chance, int rarity_tier, int level)
{
	switch(level)
	{
		// level 1:
		// - 5 items
		//   - 3 common (20 each)
		//   - 2 uncommon (20 each) 
		case(1):
			return previous_loot_chance + 20;	
			break;
		// level 2:
		// - 6 items
		//   - 3 common (20 each)
		//   - 2 uncommon (10 each) 
		//   - 1 rare (10 each)
		case(2):
			{
				if(rarity_tier == RARITY_COMMON)
				{
					return previous_loot_chance+20;
				}
				else
				{
					return previous_loot_chance+10;
				}
			}
			break;
	}
}

void Lootgen::dumpLootTable()
{
	log("dump","------- Loot table -----------");
	for(int i=0; i<loot_table.size(); i++)
	{
		log("dump", "id: " + to_string(loot_table[i].id));
		log("dump", "range_low: " + to_string(loot_table[i].range_low));
		log("dump", "range_high: " + to_string(loot_table[i].range_low));
		for(int j=0; j<loot_table[i].mods.size(); j++)
		{
			log("dump", "mod id: " + to_string(loot_table[i].mods[j].id));
			log("dump", "mod rarity: " + to_string(loot_table[i].mods[j].rarity));
		}
		log("dump","--------------------------");
	}
}

/*******************************************************************************
 * Class Mod
 *******************************************************************************/

Mod::Mod(int input_id):
	id(input_id)
{
	switch(id)
	{
		case BOOST_SPEED:
		   name = "to movement speed";
	   	break;
		case BOOST_GOLD:
		   name = "gold";
	   	break;
		case BOOST_LIFE:
		   name = "life";
	   	break;
		case BOOST_MAX_LIFE:
		   name = "to life";
	   	break;
		case BOOST_MIN_DAMAGE:
		   name = "to minimum damage";
	   	break;
		case BOOST_MAX_DAMAGE:
		   name = "to maximum damage";
	   	break;
		case WEAP_STOMP:
		   name = "Skill: Stomp";
	   	break;
		case WEAP_MISSILE:
		   name = "Weapon: Missile";
	   	break;
		case WEAP_BOMB:
		   name = "Weapon: Bomb";
	   	break;
		case WEAP_SPRINT:
		   name = "Skill: Sprint";
	   	break;
	}
}
