#include <iostream>
#include <vector>
#include <SDL_image.h>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/anim.h"
#include "../headers/actor.h"
#include "../headers/map.h"
#include "../headers/levelDebug.h"
#include "../headers/item.h"

using namespace std;

LevelDebug::LevelDebug(SDL_Renderer *renderer) : Scene(renderer)
{
	this->setName("debug");

	font = new Font("strider2", renderer);
	earth_tex = loadTexture("gfx/background_menu1.png", renderer);

	w = 388;
	h = 421;
	x = 400;
	y = 200;
}

void LevelDebug::display()
{
	// debug background
	SDL_Rect toto = {0, 0, 800, 512};

	SDL_SetRenderDrawColor(this->getRenderer(), 128, 55, 128, 255);
	SDL_RenderFillRect(this->getRenderer(), &toto);
	SDL_SetRenderDrawColor(this->getRenderer(), 0, 0, 0, 255);

	renderTexture(earth_tex, this->getRenderer(), x,y);

	if(x + w > SCREEN_W)
	{
		x = x-2;
		y = y + random(-2,2);
	}
	else if (x < 0)
	{
		x = x+2;
		y = y + random(-2,2);
	}
	else if(y + h > SCREEN_H)
	{
		x = x + random(-2, 2);
		y = y-2;
	}
	else if (y < 0)
	{
		x = x + random(-2,2);
		y = y+2;
	}
	else
	{
		x = x+2;
		y = y+2;
	}
	
	debugFonts();

	font->setText("Welcome to the debug screen!", 2);
	font->display(300,220);
	font->setText("This game has been developed by", 2);
	font->display(300,236);
	font->setText("HED854", 2);
	font->display(300,252);
	//if(item1->isGenerated())
	//{
	//	item1->displayGround(this->getRenderer(), item_sheet);
	//	item1->displayInventory(this->getRenderer(), 200, 300);
	//}

	// Display items
	//if(!items.empty())
	//{
	//	for(unsigned int i=0; i<items.size(); i++)
	//	{
	//		this->items[i]->display(this->getRenderer());
	//	}
	//}
}

Scene_chunk LevelDebug::handleKeydown(SDL_Keycode key)
{
	Scene_chunk scene_info;
	switch(key)
	{
		case SDLK_ESCAPE:
		{
			scene_info.name = "main_menu";
			return scene_info;
		}
		break;

		default:
			scene_info.empty = true;
		break;
	}

	return scene_info;
}

void LevelDebug::handleKeyup(SDL_Keycode key)
{
			//int random_x = random(0, GAME_W);
			//int random_y = random(0, GAME_H);
			//Item *test = lootgen->getLoot(0, this->loot_table, 0, random_x, random_y);
			//log("leveldebug", "safe return");
			//if(test)
			//{
			//	log("levelDebug", test->getName() + " on board");
			//	this->items.push_back(test);
			//}
			//else
			//{
			//	log("levelDebug", "nodrop");
			//}

}

void LevelDebug::cleanup()
{
	font->cleanup();
}

void LevelDebug::debugFonts()
{
	// debug fonts
	font->setText("0123456789\\ ABCDEFGHIJKLMNOP", 1);
	font->display(128,120);
	font->setText("QRSTUVWXYZ\" abcdefghijklmnop", 1);
	font->display(128,128);
	font->setText("qrstuvwxyz ,-_*()+!?", 1);
	font->display(128,136);
}
