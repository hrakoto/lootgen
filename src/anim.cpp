#include <iostream>
#include <vector>
#include <SDL_image.h>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/anim.h"

using namespace std;


Anim::Anim(string anim_name, int sheet_x, int sheet_y, int anim_w, int anim_h, int anim_frames)
	: name(anim_name),
	sprite_w(anim_w),
	sprite_h(anim_h),
	frames(anim_frames)
{
	data.clear();

	// Slicing the animation sheet!
	for(int i = 0; i < anim_w * anim_frames; i++)
	{
		SDL_Rect tmp = { sheet_x + (i * sprite_w), sheet_y, sprite_w, sprite_h };
		data.push_back(tmp);
	}
	
	// no error handling though...
}

