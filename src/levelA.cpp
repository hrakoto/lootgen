#include <iostream>
#include <vector>
#include <SDL_image.h>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/anim.h"
#include "../headers/player.h"
#include "../headers/map.h"
#include "../headers/levelA.h"

using namespace std;

LevelA::LevelA(SDL_Renderer *renderer, int input_population) : 
	Scene(renderer),
	level_map(nullptr),
	population(input_population)
{
	this->setName("fields_level");
	log("levelA", "Constructing " + this->getName());

	// Map
	tileset_tex = loadTexture("gfx/tileset_fields.png", renderer);
	vector<int> solid = { 1, 2, 3, 6, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28 };

	// There's an additional row which will get behind the GUI
	level_map = new Map(tileset_tex, 6, 5, TILE_W, TILE_H, 25, 16, solid);

	// Player
	SDL_Point playerSpawn = { 128, 128 };
	player = new Player(renderer, BASIC_SPEED - 4, BASIC_LIFE, playerSpawn, "idle");

	// Monster
	SDL_Rect monster_zone = { 500, 200, 50, 50 };
	for(int i=0; i<population; i++)
	{
		SDL_Point monsterSpawn = { 
			random(monster_zone.x, monster_zone.x + monster_zone.w),
		   	random(monster_zone.y, monster_zone.y + monster_zone.h) };
		monster.push_back(new Monster(
			renderer, 
			"mario", 
			BASIC_SPEED - 3, 
			BASIC_LIFE, 
			monsterSpawn));
	}

	// GUI
	GUI = new Gui(renderer);

	// Map
	vector< vector<int> > predefined_zones;
	vector<int> z1 = { 26,3,22,7,8,10,26,8,0 };
	vector<int> z2 = { 26,7,15,3,13,13,28,24,21 };
	vector<int> z3 = { 14,29,29,19,21,29,28,28,21 };
	vector<int> z4 = { 23,24,10,22,24,23,20,23,22 };
	vector<int> z5 = { 23,21,23,25,22,24,24,22,23 };
	vector<int> z6 = { 25,19,18,18,3,4,3,14,28 };
	vector<int> z7 = { 7,18,18,7,15,18,7,26,7 };
	vector<int> z8 = { 28,29,18,29,7,8,19,13,14 };
	vector<int> z9 = { 24,20,24,28,22,22,24,23,28 };
	vector<int> z10 = { 24,0,18,21,7,8,7,14,10 };
	vector<int> z11 = { 25,25,24,24,18,19,24,25,25 };
	predefined_zones.push_back(z1);
	predefined_zones.push_back(z2);
	predefined_zones.push_back(z3);
	predefined_zones.push_back(z4);
	predefined_zones.push_back(z5);
	predefined_zones.push_back(z6);
	predefined_zones.push_back(z7);
	predefined_zones.push_back(z8);
	predefined_zones.push_back(z9);
	predefined_zones.push_back(z10);
	predefined_zones.push_back(z11);

	level_map->generateMap(playerSpawn, predefined_zones);

	// Items
	item_sheet = loadTexture("gfx/items.png", renderer);
	drop_coords = { 0, 0 };
	lootgen = new Lootgen(1);
}

void LevelA::display()
{
	// Map
	level_map->renderMap(this->getRenderer(), 0,0);


	// Monster
	for(unsigned int i=0; i< monster.size(); i++)
	{
		if(monster[i]->getLife() > 0)
		{
			Actor_chunk next_chunk = monster[i]->move();
			monster[i]->setX(next_chunk.coords.x);
			monster[i]->setY(next_chunk.coords.y);
			monster[i]->display(this->getRenderer(), monster[i]->getX(), monster[i]->getY());
			monster[i]->changeAnim(next_chunk.anim);
		}
		else
		{
			log("levelA", "monster " + to_string(i) + " is dead!");

			// prepare drop coords
			drop_coords = { monster[i]->getX(), monster[i]->getY() };

			// get rid of monster
			monster[i]->cleanup();	
			monster.erase(monster.begin() + i);
		}
	}

	// Player
	if(player->getLife() > 0)
	{
		// This displays the player AND the missiles lol
		player->display(this->getRenderer(), player->getX(), player->getY());
	}
	else
	{
		GUI->addMessage("You die...");
	}

	// HUD
	GUI->display();
	GUI->displayLife(player->getLife());
	GUI->displayGold(player->getGold());
	GUI->displaySpeed(player->getSpeed());
	GUI->displayMessages();
	
	// Item
	for(unsigned int j=0; j<item_ground.size(); j++)
	{
		item_ground[j].displayGround(this->getRenderer(), this->item_sheet);
	}
}

void LevelA::handleRepeatKeys()
{
	if(GUI->getInventoryStatus() == false)
	{
		const Uint8* isPressed = SDL_GetKeyboardState(NULL);

		if(isPressed[SDL_SCANCODE_UP])
		{
			player->move(DIR_UP);
			player->setLastDirection(DIR_UP);
		}
		if(isPressed[SDL_SCANCODE_RIGHT])
		{
			player->move(DIR_RIGHT);
			player->setLastDirection(DIR_RIGHT);
		}
		if(isPressed[SDL_SCANCODE_DOWN])
		{
			player->move(DIR_DOWN);
			player->setLastDirection(DIR_DOWN);
		}
		if(isPressed[SDL_SCANCODE_LEFT])
		{
			player->move(DIR_LEFT);
			player->setLastDirection(DIR_LEFT);
		}
	}
}

Scene_chunk LevelA::handleKeydown(SDL_Keycode key)
{
	Scene_chunk scene_info;

	switch(key)
	{
		case SDLK_i:
			{
				if(GUI->getInventoryStatus() == false)	
				{
					GUI->setInventoryStatus(true);
				}
				else
				{
					GUI->setInventoryStatus(false);
				}
				scene_info.empty = true;
				return scene_info;
			}
		break;

		case SDLK_w:
			scene_info.empty = true;
			player->fire(player->getLastDirection());
		break;

		case SDLK_ESCAPE:
			scene_info.empty = false;
			scene_info.name = "select_menu";
			return scene_info;
		break;
		default:
			scene_info.empty = true;
			return scene_info;
		break;
	}

}

void LevelA::compute()
{
	// Generate drops if there is one
	if(drop_coords.x != 0 && drop_coords.y != 0)
	{
		item_ground.push_back(lootgen->generateItem(drop_coords.x, drop_coords.y));
	}
	drop_coords = { 0, 0 };
}

void LevelA::checkCollisions()
{
	// Missile -> Monster
	for(int i=0; i< (unsigned)monster.size(); i++)
	{
		// missileHit is here to avoid killing monsters in one blow (since the func runs a bazillons times)
		if(SDL_HasIntersection(player->getMissileHitbox(), monster[i]->getHitbox()) 
			&& !player->hasMissileHit())
		{
			GUI->addMessage("Hit a monster for " + to_string(player->getDamage()) + " dmg! ROAR!");
			monster[i]->setLife(monster[i]->getLife() - player->getDamage());
			player->setMissileHit(true);
			player->destroyMissile();
		}
	}

	// Player -> Drop
	for(int j=0; j<(unsigned)item_ground.size(); j++)
	{
		if(SDL_HasIntersection(player->getHitbox(), item_ground[j].getHitbox()))
		{
			vector<Effect_chunk> item_effects;
			item_effects = item_ground[j].getEffects();

			for(int k=0; k<item_effects.size(); k++)
			{
				if(item_effects[k].type == EFFECT_IMMEDIATE)
				{
					player->setStat(item_effects[k].stat, item_effects[k].value);
					GUI->addMessage(item_effects[k].message);
				}
				else
				{
					GUI->addMessage("You pick up " + item_ground[j].getName());
					GUI->addInventory(item_ground[j]);
				}
			}
			// In both cases, remove from the ground
			item_ground.erase(item_ground.begin() + j);
		}
	}
}

void LevelA::handleKeyup(SDL_Keycode key)
{
	if(GUI->getInventoryStatus() == true)
	{
		key = SDLK_DOWN;
	}
	switch(key)
	{
		case SDLK_RIGHT:
		case SDLK_LEFT:
		case SDLK_UP:
		case SDLK_DOWN:
			player->changeAnim("idle");
		break;

		default:
		break;

	}
}
void LevelA::cleanup()
{
	SDL_DestroyTexture(tileset_tex);
	GUI->cleanup();
	player->cleanup();
	SDL_DestroyTexture(item_sheet);
	//monster->cleanup();
}
