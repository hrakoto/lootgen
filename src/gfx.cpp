#include <iostream>
#include <vector>
#include <cmath>
#include <SDL_image.h>
#include "../headers/tools.h"
#include "../headers/sdl_debug.h"
#include "../headers/gfx.h"

using namespace std;

/*
 * Loads a png
 */
SDL_Texture* loadTexture(const string &file, SDL_Renderer *renderer)
{
	SDL_Surface* surface = IMG_Load(file.c_str());
	//log("gfx", file.c_str());
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
	if(texture == nullptr)
	{
		log("error", SDL_GetError());
		return nullptr;
	}
	else
	{
		return texture;
	}
}

/*
 * Render a texture beginning at x,y
 */
void renderTexture(SDL_Texture *texture, SDL_Renderer *renderer, int x, int y)
{
	SDL_Rect dst;
	dst.x = x;
	dst.y = y;
	// update width and height with read texture attributes
	SDL_QueryTexture(texture, NULL, NULL, &dst.w, &dst.h);
	//log("gfx", "renderTexture: " + to_string(texture->w) + "x" + to_string(texture->h) + " to: " + to_string(x) + "," + to_string(y));
	SDL_RenderCopy(renderer, texture, NULL, &dst);
}

/*
 * Render a tile from texture, situated at clip, at x,y
 */
void renderTile(SDL_Texture *texture, SDL_Renderer *renderer, int tile_w, int tile_h, int num_x, int num_y, int dst_x, int dst_y, int zoom)
{
	//log("gfx", "renderTile: " + to_string(num_x) + "," + to_string(num_y) + " to: " + to_string(dst_x) + "," + to_string(dst_y));
	// src rect. We pick the right tile in the spritesheet
	SDL_Rect clip;
	clip.w = tile_w; 
	clip.h = tile_h;
	clip.x = num_x * tile_w;
	clip.y = num_y * tile_h;

	// destination rect. We can resize the tile here.
	SDL_Rect dst;
	dst.x = dst_x;
	dst.y = dst_y;
	dst.w = clip.w * zoom; 
	dst.h = clip.h * zoom; 

	SDL_RenderCopy(renderer, texture, &clip, &dst);
}

void renderAnimation(SDL_Texture *texture, SDL_Renderer *renderer, vector<SDL_Rect> anim_frames, int frame, int dst_x, int dst_y)
{
	SDL_Rect dst;
	dst.x = dst_x;
	dst.y = dst_y;
	dst.w = anim_frames[0].w;
	dst.h = anim_frames[0].h;

	SDL_RenderCopy(renderer, texture, &anim_frames[frame], &dst);
}

/*
 * Returns a x coordinate centered on screen
 */
int centerX(SDL_Texture *texture, int screen_w)
{
	SDL_Rect tex;
	SDL_QueryTexture(texture, NULL, NULL, &tex.w, &tex.h);
	return (int)floor((screen_w - tex.w) / 2);
}
int centerX(int w, int screen_w)
{
	return (int)floor((screen_w - w) / 2);
}
