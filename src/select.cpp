#include <iostream>
#include <vector>
#include <SDL_image.h>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/map.h"
#include "../headers/select.h"
#include "../headers/font.h"

using namespace std;

const int MARGIN_LEFT = 64;

Select::Select(SDL_Renderer *renderer) : Scene(renderer),
	fields_map(nullptr),
	mountains_map(nullptr),
	population_A(random(5,10)),
	population_B(random(10,50))
{
	this->setName("select_menu");

	// Initial menu selector position
	this->selector_index_x = 0;

	background_tex = loadTexture("gfx/background_menu.png", renderer);
	title_tex = loadTexture("gfx/select.png", renderer);
	fields_tex = loadTexture("gfx/tileset_fields.png", renderer);
	mountains_tex = loadTexture("gfx/tileset_city.png", renderer);
	selector_tex = loadTexture("gfx/select_border.png", renderer);
	fields_name = loadTexture("gfx/select_fields.png", renderer);
	mountains_name = loadTexture("gfx/select_city.png", renderer);

	// Starfield
	for(int i=0; i<5; i++)
	{
		SDL_Rect tmp = { random(0, SCREEN_W), random(285, SCREEN_H), 3, 3 };
		big_stars.push_back(tmp);
	}

	for(int j=0; j<15; j++)
	{
		SDL_Rect tmp = { random(0, SCREEN_W), random(285, SCREEN_H), 1, 1 };
		small_stars.push_back(tmp);
	}

	font = new Font("strider2", renderer);

	fields_map = new Map(fields_tex, 6, 5, TILE_W, TILE_H, 5, 4);
	mountains_map = new Map(mountains_tex, 6, 4, TILE_W, TILE_H, 5, 4);

	fields_map->generateMap();
	mountains_map->generateMap();
}

void Select::display()
{
	renderTexture(background_tex, this->getRenderer(), 0,0);

	// Starfield
	SDL_SetRenderDrawColor(this->getRenderer(), 255, 255, 255, 255);
	for(unsigned int i=0; i<big_stars.size(); i++)
	{
		big_stars[i].x = big_stars[i].x + 2;
		if(big_stars[i].x > SCREEN_W)
		{
			big_stars[i].x = 0;
		}
		SDL_RenderFillRect(this->getRenderer(), &big_stars[i]);
	}
	for(unsigned int j=0; j<small_stars.size(); j++)
	{
		small_stars[j].x = small_stars[j].x + 1;
		if(small_stars[j].x > SCREEN_W)
		{
			small_stars[j].x = 0;
		}
		SDL_RenderFillRect(this->getRenderer(), &small_stars[j]);
	}
	renderTexture(title_tex, this->getRenderer(), centerX(title_tex, SCREEN_W),64);

	// Level preview
	fields_map->renderMap(this->getRenderer(), MARGIN_LEFT,128);
	renderTexture(fields_name, this->getRenderer(), MARGIN_LEFT ,128 + (4 * TILE_H));

	mountains_map->renderMap(this->getRenderer(), MARGIN_LEFT + (5 * TILE_W) + 64 ,128);
	renderTexture(mountains_name, this->getRenderer(), MARGIN_LEFT + (5 * TILE_W) + 64 ,128 + (4 * TILE_H));

	font->setText("Population: " + to_string(population_A), 1);
	font->display(64, 292);
	font->setText("Population: " + to_string(population_B), 1);
	font->display(288, 292);

	this->displaySelector();
}

void Select::displaySelector()
{
	// The selector has two valid positions atm:
	int selector_pos_x;
	int selector_pos_y;
	if(this->selector_index_x == 0)
	{
		selector_pos_x = MARGIN_LEFT - 4;
		selector_pos_y = 128 - 4;
	}
	else
	{
		selector_pos_x = MARGIN_LEFT + (5 * TILE_W) + 64 - 4;
		selector_pos_y = 128 - 4;
	}

	renderTexture(selector_tex, this->getRenderer(), selector_pos_x, selector_pos_y);
}

Scene_chunk Select::handleKeydown(SDL_Keycode key)
{
	Scene_chunk scene_info;
	switch(key)
	{
		case SDLK_LEFT:
			if(selector_index_x == 1)
			{
				this->clearSelector();
				selector_index_x = 0;
				this->displaySelector();
			}
			scene_info.empty = true;
			return scene_info;
		break;
		case SDLK_RIGHT:
			if(selector_index_x == 0)
			{
				this->clearSelector();
				selector_index_x = 1;
				this->displaySelector();
			}
			scene_info.empty = true;
			return scene_info;
		break;
		case SDLK_RETURN:
			if(selector_index_x == 0)
			{
				scene_info.name = "levelA";
				scene_info.empty = false;
				scene_info.population = population_A;
				return scene_info;
			}
			else
			{
				scene_info.empty = true;
				return scene_info;
			}
		break;
		case SDLK_ESCAPE:
			scene_info.name = "main_menu";
			scene_info.empty = false;
			return scene_info;
		break;
		default:
			scene_info.empty = true;
			return scene_info;
		break;
	}
}

/*
 * Partial rendering used to clear the old selector position
 */
void Select::clearSelector()
{
	SDL_SetRenderTarget(this->getRenderer(), selector_tex);
	SDL_RenderClear(this->getRenderer());
	SDL_SetRenderTarget(this->getRenderer(), nullptr);
}

void Select::cleanup()
{
	SDL_DestroyTexture(selector_tex);
	SDL_DestroyTexture(fields_name);
	SDL_DestroyTexture(mountains_name);
	SDL_DestroyTexture(mountains_tex);
	SDL_DestroyTexture(fields_tex);
	SDL_DestroyTexture(title_tex);
	SDL_DestroyTexture(background_tex);
}
