#include <iostream>
#include <SDL.h>
#include <SDL_image.h>

#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/map.h"
#include "../headers/scene.h"
#include "../headers/menu.h"
#include "../headers/select.h"
#include "../headers/levelA.h"
#include "../headers/levelDebug.h"

using namespace std;

SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;

// Scene management
Scene *current_scene = nullptr;
Scene_chunk next_scene_info;
void changeScene(Scene_chunk next_scene);
Scene* factory(Scene_chunk scene_info);

void cleanup();
int init();
void loadAssets();

int main(int, char**)
{
	init();	

	//loadAssets();

	bool loop = true;
	Scene_chunk scene_info;
	scene_info.name = "main_menu";
	current_scene = factory(scene_info);

	// render static elements once
	while(loop)
	{
		current_scene->display();
		SDL_RenderPresent(renderer);


		SDL_Event event;
		current_scene->handleRepeatKeys();
		while(SDL_PollEvent(&event))
		{
			if(event.type == SDL_QUIT)
			{
				loop = false;
			}
			else if(event.type == SDL_KEYDOWN)
			{
				next_scene_info = current_scene->handleKeydown(event.key.keysym.sym);
				if(next_scene_info.empty != true)
				{
					changeScene(next_scene_info);
				}
			}
			else if(event.type == SDL_KEYUP)
			{
				current_scene->handleKeyup(event.key.keysym.sym);
			}
		}
		current_scene->compute();
		current_scene->checkCollisions();

		SDL_RenderClear(renderer);
		SDL_Delay(16);
	}	
}

int init()
{
	log("main", "Initializing...");
	if(SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		log("error", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	// Create window
	window = SDL_CreateWindow(GAME_NAME, 100,100,SCREEN_W,SCREEN_H, SDL_WINDOW_SHOWN);
	if(window == nullptr)
	{
		log("error", SDL_GetError());
		SDL_Quit();
		return 1;
	}
	
	// Create renderer
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if(renderer == nullptr)
	{
		SDL_DestroyWindow(window);
		log("error", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	return 0;
}


void changeScene(Scene_chunk next_scene)
{
	log("main", "Request to change scene to: " + next_scene.name);
	if(current_scene->getName() != next_scene.name)
	{
		SDL_RenderClear(renderer);
		current_scene->cleanup();
		current_scene = factory(next_scene);
	}
}

/*
 * Builds the right scene according to name
 */
Scene* factory(Scene_chunk scene_info)
{
	if(scene_info.name == "main_menu")
	{
		return new Menu(renderer);
	}
	else if(scene_info.name == "select_menu")
	{
		return new Select(renderer);
	}
	else if(scene_info.name == "debug")
	{
		return new LevelDebug(renderer);
	}
	else if(scene_info.name == "levelA")
	{
		return new LevelA(renderer, scene_info.population);
	}
	else
	{
		return nullptr;
	}
}


void loadAssets()
{
}

void cleanup()
{
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
}


