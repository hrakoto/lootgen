#include <iostream>
#include <vector>
#include <cmath>
#include <typeinfo>
#include <cmath>
#include <SDL_image.h>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/font.h"
#include "../headers/gui.h"

using namespace std;

Gui::Gui(SDL_Renderer *input_renderer) :
	renderer(input_renderer),
	display_inventory(false),
	message_x(532),
	message_y(512)
{
	gui_tex = loadTexture("gfx/gui.png", input_renderer);
	font = new Font("strider2", input_renderer);

	// Inventory
	inventory_global = { 0, 0, SCREEN_W, 113 };
	inventory_item = { 8, 8, 372, 113 - 16 };
	inventory_list = { 420, 8, 372, 113 - 16 };
}

void Gui::display()
{
	renderTexture(gui_tex, renderer, 0,482);
	if(getInventoryStatus() == true)
	{
		SDL_SetRenderDrawColor(this->getRenderer(), 63, 61, 61, 255);
		SDL_RenderFillRect(this->getRenderer(), &inventory_global);
		SDL_SetRenderDrawColor(this->getRenderer(), 0, 0, 0, 255);
		SDL_RenderFillRect(this->getRenderer(), &inventory_item);
		SDL_RenderFillRect(this->getRenderer(), &inventory_list);

		for(int i=0; i<displayed_items.size(); i++)
		{
			displayed_items[i].displayInventoryName(this->getRenderer(), 8, 8 +(16*i));
		}
	}
}

void Gui::addMessage(string message)
{
	// if display is full: remove the oldest line
	if(displayed_messages.size() == 7)
	{
		pop_front(displayed_messages);
	}
	// in all cases, add the new line
	displayed_messages.push_back(message);
}

void Gui::addInventory(Item item)
{
	displayed_items.push_back(item);
}

void Gui::displayMessages()
{
	for(int i=0; i<displayed_messages.size(); i++)
	{
		font->setText(displayed_messages[i], 1);
		font->display(message_x, message_y + 8*i);
	}
}

void Gui::displayGold(int gold)
{
	font->setText("Gold: " + to_string(gold), 2);
	font->display(200, 514);
}

void Gui::displayLife(int life)
{
	font->setText("Life: " + to_string(life), 2);
	font->display(200, 530);
}
void Gui::displaySpeed(int speed)
{
	font->setText("Speed: " + to_string(speed), 2);
	font->display(200, 546);
}
void Gui::cleanup()
{
	SDL_DestroyTexture(gui_tex);
	font->cleanup();
}

