#include <iostream>
#include <vector>
#include <SDL_image.h>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/map.h"
#include "../headers/menu.h"

using namespace std;

Menu::Menu(SDL_Renderer *renderer) : Scene(renderer)
{
	this->setName("main_menu");

	background_tex = loadTexture("gfx/background_menu.png", renderer);
	background_earth = loadTexture("gfx/background_menu1.png", renderer);
	title_tex = loadTexture("gfx/title.png", renderer);
	tileset_tex = loadTexture("gfx/tilesets.png", renderer);

	for(int i=0; i<5; i++)
	{
		SDL_Rect tmp = { random(0, SCREEN_W), random(285, SCREEN_H), 3, 3 };
		big_stars.push_back(tmp);
	}

	for(int j=0; j<15; j++)
	{
		SDL_Rect tmp = { random(0, SCREEN_W), random(285, SCREEN_H), 1, 1 };
		small_stars.push_back(tmp);
	}




	font = new Font("strider2", renderer);
}

void Menu::display()
{
	renderTexture(background_tex, this->getRenderer(), 0,0);

	// Starfield
	SDL_SetRenderDrawColor(this->getRenderer(), 255, 255, 255, 255);
	for(unsigned int i=0; i<big_stars.size(); i++)
	{
		big_stars[i].x = big_stars[i].x + 2;
		if(big_stars[i].x > SCREEN_W)
		{
			big_stars[i].x = 0;
		}
		SDL_RenderFillRect(this->getRenderer(), &big_stars[i]);
	}
	for(unsigned int j=0; j<small_stars.size(); j++)
	{
		small_stars[j].x = small_stars[j].x + 1;
		if(small_stars[j].x > SCREEN_W)
		{
			small_stars[j].x = 0;
		}
		SDL_RenderFillRect(this->getRenderer(), &small_stars[j]);
	}
	SDL_SetRenderDrawColor(this->getRenderer(), 0, 0, 0, 255);

	renderTexture(background_earth, this->getRenderer(), 411, 138);

	renderTexture(title_tex, this->getRenderer(), 64, 64);

	font->setText("v0.1 - Segmentation fault forever", 1);
	font->display(64, 128);

	font->setText("Press ENTER to play", 2);
	font->displayBox(64, 400);
}

Scene_chunk Menu::handleKeydown(SDL_Keycode key)
{
	Scene_chunk scene_info;
	switch(key)
	{
		case SDLK_RETURN:
			scene_info.name = "select_menu";
			scene_info.empty = false;
			return scene_info;
		break;
		case SDLK_HOME:
			scene_info.name = "debug";
			scene_info.empty = false;
			return scene_info;
		break;
		default:
			scene_info.empty = true;
			return scene_info;
		break;
	}
}

void Menu::cleanup()
{
	SDL_DestroyTexture(background_tex);
	SDL_DestroyTexture(title_tex);
	SDL_DestroyTexture(tileset_tex);
}
