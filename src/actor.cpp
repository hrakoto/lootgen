#include <iostream>
#include <vector>
#include <cmath>
#include <SDL_image.h>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/anim.h"
#include "../headers/actor.h"

using namespace std;

Actor::Actor(SDL_Texture *texture,  int input_w, int input_h) : 
	actor_tex(texture),
	current_frame(0),
	tempo_frame(0)
{
	this->hitbox.w = input_w;
	this->hitbox.h = input_h;
}

void Actor::addAnim(string name, int x, int y, int w, int h, int frames)
{
	this->anims[name] = new Anim(name, x, y, w, h, frames);
}

void Actor::display(SDL_Renderer *renderer, int new_x, int new_y)
{
	this->setPositionXY(new_x, new_y);
	renderAnimation(actor_tex, renderer, this->getCurrentAnim()->getAnimData(), current_frame, pos_x, pos_y);

	tempo_frame++;

	// Updating real current frame 4x slower
	if(floor(tempo_frame / 4) > current_frame)
	{
		current_frame++;
	}

	// We have 4x frames before reset
	if(tempo_frame / 4 >= this->getCurrentAnim()->getFrames() )
	{
		// reset animation
		current_frame = 0;
		tempo_frame = 0;
	}

}

void Actor::changeAnim(string next_anim_name)
{
	Anim *next_anim = this->anims[next_anim_name];

	if(this->getCurrentAnim() != next_anim)
	{
		this->setCurrentAnim(next_anim);
	}
}

void Actor::setCurrentAnim(Anim *anim = nullptr)
{
	if(anim)
	{
		this->current_anim = anim;
	}
}

void Actor::setPositionXY(int x, int y)
{
	this->pos_x = x;
	this->pos_y = y;
	this->hitbox.x = x;
	this->hitbox.y = y;
}

void Actor::setPositionX(int x)
{
	this->pos_x = x;
	this->hitbox.x = x;
}

void Actor::setPositionY(int y)
{
	this->pos_y = y;
	this->hitbox.y = y;
}

Behavior* Actor::setBehavior(string input_name)
{
	if(input_name == "stupid")
	{
		return new StupidAI(this->pos_x, this->pos_y);
	}
	else
	{
		// TODO: throw exception
		return nullptr;
	}
}


void Actor::cleanup()
{
	SDL_DestroyTexture(actor_tex);
}

/*******************************************************************************
 * STUPID BEHAVIOR CLASS
 *******************************************************************************/

StupidAI::StupidAI(int actor_x, int actor_y)
	: current_dir(2)
{
	last_position = { actor_x, actor_y };
	cur_chunk.coords.x = actor_x;
	cur_chunk.coords.y = actor_y;
}

Actor_chunk StupidAI::move(int speed)
{
	Actor_chunk next_chunk;
	int direction;
	int distance = random(1, 100);

	// Checking the extremes
	if(cur_chunk.coords.y < 0)
	{
		direction = DIR_DOWN;
	}
	else if(cur_chunk.coords.x > GAME_W - 32)
	{
		direction = DIR_LEFT;
	}
	else if(cur_chunk.coords.y > GAME_H - 32)
	{
		direction = DIR_UP;
	}
	else if(cur_chunk.coords.x < 0)
	{
		direction = DIR_RIGHT;
	}
	else
	{
		// within bounds:
		// 1) target reached
		if(current_dir == 1000)
		{
			direction = random(0, 3);
		}
		// 2) continue to reach target
		else
		{
			direction = this->current_dir;
		}
	}


	switch(direction)
	{
		// up
		case DIR_UP:
			if(cur_chunk.coords.y >= last_position.y - distance)
			{
				cur_chunk.coords.y = cur_chunk.coords.y - speed;
				next_chunk.anim = "walk_up";
				this->current_dir = DIR_UP;
			}
			else
			{
				last_position.y = cur_chunk.coords.y; 
				this->current_dir = 1000;
			}
		break;

		// right
		case DIR_RIGHT:
			if(cur_chunk.coords.x <= last_position.x + distance )
			{
				cur_chunk.coords.x = cur_chunk.coords.x + speed;
				next_chunk.anim = "walk_right";
				this->current_dir = DIR_RIGHT;
			}
			else
			{
				last_position.x = cur_chunk.coords.x;
				this->current_dir = 1000;
			}
		break;

		// down
		case DIR_DOWN:
			if(cur_chunk.coords.y <= last_position.y + distance )
			{
				cur_chunk.coords.y = cur_chunk.coords.y + speed;
				next_chunk.anim = "walk_down";
				this->current_dir = DIR_DOWN;
			}
			else
			{
				last_position.y = cur_chunk.coords.y;
				this->current_dir = 1000;
			}
		break;

		// left
		case DIR_LEFT:
		if(cur_chunk.coords.x >= last_position.x - distance)
			{
				cur_chunk.coords.x = cur_chunk.coords.x - speed;
				next_chunk.anim = "walk_left";
				this->current_dir = DIR_LEFT;
			}
			else
			{
				last_position.x = cur_chunk.coords.x;
				this->current_dir = 1000;
			}
		break;
	}
	next_chunk.coords = { cur_chunk.coords.x, cur_chunk.coords.y };
	return next_chunk;
}
