#include "../headers/constants.h"
#include <string>

using namespace std;

const char *GAME_NAME = "lootgen";
const int SCREEN_W = 800;
const int SCREEN_H = 600;
const int TILE_W = 32;
const int TILE_H = 32;
const int GAME_W = 800;
const int GAME_H = 480; 

const int BASIC_LIFE = 100;
const int BASIC_SPEED = 5;
const int BASIC_MISSILE_TIMER = 2;
const int BASIC_MISSILE_MIN_DAMAGE = 35;
const int BASIC_MISSILE_MAX_DAMAGE = 35;

//const string FILE_TIERDATA = "tier.dat";
//const char KEY_TIERDATA = 'T';
