#include <iostream>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include <SDL_image.h>
#include "../headers/constants.h"
#include "../headers/tools.h"
#include "../headers/gfx.h"
#include "../headers/map.h"

using namespace std;

/*
 * Idée pour map: générer des clumps de 3x3, les selectionner
 * les déplacer de x,y random (utiliser une seed random et utiliser chauqe nombre)
 * et remplir les trous avec du vert
 */

Map::Map(SDL_Texture *texture, int num_tiles_x, int num_tiles_y, int clip_w, int clip_h, int map_w, int map_h, int mult)
	: spritesheet(texture),
	spritesheet_tiles_x(num_tiles_x),
	spritesheet_tiles_y(num_tiles_y),
	tile_w(clip_w),
	tile_h(clip_h),
	w(map_w),
	h(map_h),
	zoom(mult)
{
 
}

Map::Map(SDL_Texture *texture, int num_tiles_x, int num_tiles_y, int clip_w, int clip_h, int map_w, int map_h, vector<int> solid, int mult)
	: spritesheet(texture),
	spritesheet_tiles_x(num_tiles_x),
	spritesheet_tiles_y(num_tiles_y),
	tile_w(clip_w),
	tile_h(clip_h),
	w(map_w),
	h(map_h),
	zoom(mult)
{
	solid_index = solid;	
}


void Map::generateMap()
{
	// do not create a huge mapindex with successive calls.
	mapindex.clear();

	// first pass = random crap
	for(int j = 0; j < h; j++)
	{
		vector<int> tmp;
		for(int i = 0; i < w; i++)
		{
			int index = random(0, (spritesheet_tiles_x * spritesheet_tiles_y) - 1 );
			tmp.push_back(index);
		}
		mapindex.push_back(tmp);
	}
}

void Map::generateMap(SDL_Point spawn_zone, vector< vector<int> > addon_zones)
{
	this->generateMap();

	// second pass = generate a spawn zone
	vector<int> zone = {24,9,9, 9,24,23, 24,23,24};
	addZone(spawn_zone, zone);

	// And add additional zones
	int nb_addon_zones = addon_zones.size();
	for(int i=0; i < random(10,15); i++)
	{
		// The addon zones are always 3x3
		SDL_Point origin_zone = { random(0, GAME_W - 3 * TILE_W), random(0, GAME_H - 3 * TILE_H)  };
		int zone_to_add = random(0, nb_addon_zones - 1);
		addZone(origin_zone, addon_zones[zone_to_add]);
	}
}

void Map::addZone(SDL_Point zone_start, vector<int> zone)
{
	int index_x = (int) floor(zone_start.x / tile_w);
	int index_y = (int) floor(zone_start.y / tile_w);

	for(int i=0; i < 3; i++)
	{
		for(int j=0; j < 3; j++)
		{
			mapindex[index_y + j][index_x + i] = zone[i + 3*j];
		}
	}

}

void Map::renderMap(SDL_Renderer *renderer, int origin_x, int origin_y)
{
	int num_x;
	int num_y;	
	int dst_x;
	int dst_y;
	for(int j = 0; j < h; j++)
	{
		for(int i = 0; i < w; i++)
		{
			// tile number to indexed tile coordinates
			// note: mapindex (j) contains tmp (i) so we address with: j,i
			num_x = mapindex[j][i] % spritesheet_tiles_x;
			num_y = (int) floor(mapindex[j][i] / spritesheet_tiles_x);

			// render coordinates 
			dst_x = origin_x + (i * tile_w * zoom);
			dst_y = origin_y + (j * tile_h * zoom);

			renderTile(spritesheet, renderer, tile_w, tile_h, num_x, num_y, dst_x, dst_y, zoom);
		}
	}
}

bool Map::isSolid(int x, int y)
{
	log("map", to_string(x)  + "," + to_string(y));
	log("map", to_string(w)  + "," + to_string(h));
	if((x < w * tile_w) && (y < h * tile_h))
	{
		int index_x = (int) floor(x / tile_w);
		int index_y = (int) floor(y / tile_h);
		for(int i = 0; i < solid_index.size(); i++)	
		{
			if(this->mapindex[index_y][index_x] == solid_index[i])
			{
				return true;
			}
		}
		return false;
	}
	else
	{
		throw out_of_range("Map::isSolid: coordinates are not on this map");
	}
}
