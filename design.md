# Loot gen

Action RPG vu de haut. On joue un monstre qui envahit la Terre.
But du jeu: avoir le plus gros score de destruction au bout des 6 jours.

Jour 1: 
- On sélectionne un lvl parmi les 6.
- Il faut détruire un max de trucs le plus vite possible sous time limit. Ensuite on revientau menu.
- Si on a tué tous les monstres, on a le droit d'installer un batiment: (Forge / QG / Jardin) (seule la forge est dispo ds la version rbdgame) et d l'utiliser tout de suite.
- Si onn'a pas fait un full clear: niveau suivant.
- qd tous les niveaux sont épuisés, le score est donné.

Le QG:
- redonne tous les HP
- ouvre les events entre les journées

La forge mystique:
- on met n'importe quoi dedans x3 et ca donne 1 item
- input et output: armes et bouffe
- output immediat

La plantation:
- input: uniquement pour la bouffe
- output: nimporte quoi
- output pour la journée suivante 

## choix techniques

- vue de haut
- pas de scrolling, comme binding of isaac ou zelda

- il faudra un modele pr un joueur ou monstre
- il faudra des IA de monstre
- il faudra differnets effets de skills
- il faudra un modèle pour les transformateurs comme la forge ou autre (input output formula)
- il faudra une difficulté qui s'adapte en fonction de la journée à laquelle le joueur attaque la zone
- il faudra une droplist differente par zone
- il faudra une formula list
- il faudra un système de scenes pour gérer le avant mission / pendant
- il faudra un système de deplacement au clavier (menus et transformateurs)

- option: ajouter une histoire (cutscenes entre chaque journée avec des citations) 
- option: random generation donjon
- option: savegame
- option: changer les touches

## par ex

Trucs que l'on peut obtenir ds la forge ou dans les events
armes:
- laser ======
- pistol
- aura
- bombardment extraterestre

bonus:
- vitesse+
- score+
- il y a des bonus négatifs

Drops rares sur les monstres
- clé pour niveau bonus
- forge miniature (a mettre ds la forge pour avoir un item perfect) 

## MVP

le systeme de scenes preparation / day 1 / preparation / day 2
un menu avec le systeme de deplacement au clavier
un perso qui se deplace et qui tire
des monstres qui meurent et droppent
la difficulté qui augmente (stats en fonction du jour)
on peut ramasser ou manger un item
des animations

## jeux inspiration

zelda (pas de scrolling mais joli overworld)
binding of issac (le perso change tres legerement en fcto de l'unique ou dernier objet?)
hero siege (on peut se coincer dans le decor, decor destructible)
